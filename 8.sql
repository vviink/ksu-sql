-- ЛР №8
-- Проектирование и проверка триггеров ( trigger), активизирующихся при добавлении (insert), исправлении (update) и удалении (delete) данных.
-- Реализовать набор триггеров, реализующих следующие деловые правила :


-- 1. Количество тем может быть в диапазоне от 5 до 10.
DELIMITER $$ -- Изменяем разделитель между коммандами с ; на $$
CREATE TRIGGER trig1 BEFORE INSERT ON `тема`
FOR EACH ROW
BEGIN
  SET @num = (SELECT COUNT(*) FROM `тема`);
  IF @num > 10 THEN
    signal sqlstate '45000' set message_text = 'Число тем больше допустимого';
  ELSEIF @num < 5 THEN
    signal sqlstate '45000' set message_text = 'Число тем меньше допустимого';
  END IF;
END;
$$
DELIMITER ; -- Изменяем разделитель между коммандами с $$ на ;


-- 2. Новинкой может быть только книга изданная в текущем году.
DELIMITER $$
CREATE TRIGGER trig2 BEFORE INSERT ON `книги`
FOR EACH ROW
BEGIN
  set @n = 'Yes';
  @y = (select year(NEW.`Дата`));
  if @y < year(NOW()) AND NEW.`Новинка` = @n THEN
    SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = 'Эта не новая книга';
  End IF;
END;
$$
DELIMITER ;


-- 3. Книга с количестовом страниц  до 100 не может стоить более 10$, до 200 – 20$, до 300 – 30$.
DELIMITER $$
CREATE TRIGGER trig3  BEFORE INSERT ON `книги`
FOR EACH ROW
BEGIN
  SET @pg = NEW.`Страницы`;
  SET @pr = NEW.`Цена`;
  if (@pg < 100 AND @pr > 10) OR
  (@pg < 200) AND @pr > 20) OR
  (@pg < 300 AND @pr > 30)
  THEN
    SET @m = 'Цена не соответствует количеству страниц';
    SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = @m;
  end if;
END
$$
DELIMITER ;


-- 4. Издательство “BHV” не выпускает книги тиражом меньшим 5000, а издательство Diasoft – 10000.
CREATE TRIGGER trig4 BEFORE INSERT  ON `книги`
FOR EACH ROW
BEGIN 
declare @p1 int, @p2 int, @p int, @e1 int, @e2 int, @e int
set @p1=1,  @p2=6, @e1=6000, @e2=10000;
select @p=id_izd, @e=`книги`.`Издательство` from `книги`
if (@p=@p1 and @e<@e1) or
(@p=@p2 and @e<@e2)
begin 
SET @m = 'Не верный тираж';
SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = @m;
end
END

-- 5.  Книги с одинаковым кодом должны иметь одинаковые данные.
CREATE TRIGGER trig5 INSERT ON `книги`
FOR EACH ROW
declare @c int
select @c=`книги`.`Код`, @p=id_b from `книги`
if (@c<>@c1 and @p<>@p1)  ////???
begin 
SET @m = 'Книги с разными кодами';
SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = @m;
end
END


-- 6. При попытке удаления книги выдается информация о количестве удаляемых строк. Если пользователь не “dbo”, то удаление запрещается.


-- 7. Пользователь “dbo” не имеет права изменять цену книги.


-- 8. Издательства ДМК и Эком учебники не издают.
CREATE TRIGGER trig8 INSERT ON `книги`
FOR EACH ROW
declare @p1 int, @p2 int, @p int, @c1 int, @c int
set @p1=7, @p2=9, @c1=1,
select @c=id_k, @p=id_izd from `книги`
if (@p=@p1 and @c=@c1) or
(@p=@p2 and @c=@c1)
begin 
SET @m = 'ДМК и Эком учебники не издают';
SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = @m;
end
END


-- 9. Издательство не может выпустить более 10 новинок в течении одного месяца текущего года.
CREATE TRIGGER trig9 BEFORE INSERT ON `книги`
FOR EACH ROW
BEGIN
declare @n char(3), @m int, @k1 int, @k int, @y int
set @n = 'Yes',
set @k1=10;
select @k=count(`id_b`) from `книги` where `книги`.`новинка`=@n, month(`Дата`)=@m1 
if (@k>k1 and (@y < year(SYSDATETIME()) or @y > year(SYSDATETIME()) and @m1<>@m )
begin 
SET @s = 'Издательство не может выпустить ';
SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = @s;
End


-- 10. Издательство BHV не выпускает книги формата 60х88/16.
CREATE TRIGGER trig10 before INSERT ON `книги`
FOR EACH ROW
declare @p1 int, @p int, @s1 char(12), @s char(12)
set @p1=1, @s1='060х088/016';
select @s= `книги`.`Формат`  from `книги` where id_izd=@p1
if (@p=@p1 and @s=@s1)
begin 
SET @m = 'Издательство BHV не выпускает книги формата 60х88/16';
SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = @m;
end
END